#include <QString>
#include <QtTest>
#include "../src/note.h"
#include "../src/tact.h"

class TestsTest : public QObject
{
    Q_OBJECT

public:
    TestsTest();

private Q_SLOTS:
    void testCase1();
    void testToNoteAddNote();
    void testCorrectFractionTime();
    void testDotes();
    void testAccent();
    void testSetAccentFromTact();
    void testPause();
    void testTactMaxSize();
    void testTactTime();
    void testTactCorrect();
    void testCopyTact();
    void testSetTactKey();
};

TestsTest::TestsTest(){
}

void TestsTest::testCase1(){
    QVERIFY2(true, "Failure");
}

void TestsTest::testToNoteAddNote(){
    Note* noteA=new Note(30);
    Note* noteB=new Note(*noteA);
    QCOMPARE(30,noteB->getSoundKey());
    noteA->setSoundKey(29);
    QCOMPARE(29,noteA->getSoundKey());
    delete noteA;
    delete noteB;
}

void TestsTest::testAccent(){
    Note* testNote=new Note();
    QCOMPARE(100,testNote->getAccent());
    testNote->setAccent(120);
    QCOMPARE(120,testNote->getAccent());
    delete testNote;
}

void TestsTest::testCorrectFractionTime(){
    FractionsContainer* whole=new FractionsContainer(256,0.0625);
    FractionsContainer* demi=new FractionsContainer(8,0.0625);
    QCOMPARE(0.0625,demi->getTime());
    QCOMPARE(2.0,whole->getTime());
    whole->changeTime(0.0375);
    demi->changeTime(0.0375);
    QCOMPARE(0.0375,demi->getTime());
    QCOMPARE(1.2,whole->getTime());
    delete whole;
    delete demi;
}

void TestsTest::testDotes(){
    FractionsContainer* quart= new FractionsContainer(64,0.0625);
    quart->setDoubleDote();
    QCOMPARE(0.875,quart->getTime());
    QCOMPARE(112,quart->getFractionSize());
    quart->setSingleDote();
    QCOMPARE(96,quart->getFractionSize());
    delete quart;
}

void TestsTest::testPause(){
    FractionsContainer* testCon= new FractionsContainer(256,0.0625);
    testCon->setNoteKey(3,32);
    QCOMPARE(false,testCon->isPause());
    testCon->setPause();
    QCOMPARE(true,testCon->isPause());
    QCOMPARE(0,testCon->getNote(3)->getSoundKey());
    delete testCon;
}

void TestsTest::testTactMaxSize(){
    Tact* testTact= new Tact(120,3,4);
    QCOMPARE(192,testTact->getMaxSize());
    testTact->setLenghtFraction(8);
    QCOMPARE(96,testTact->getMaxSize());
    testTact->setQuantityFraction(8);
    QCOMPARE(256,testTact->getMaxSize());
    delete testTact;
}

void TestsTest::testTactTime(){
    Tact* testTact= new Tact(120,3,4);
    QCOMPARE(0.0625,testTact->getUnitOfTime());
    testTact->setTactTempo(166.0);
    QCOMPARE(0.04518072289,QString::number(testTact->getUnitOfTime(), 'g', 10).toDouble());
    delete testTact;
}

void TestsTest::testTactCorrect(){
    Tact* testTact= new Tact(120,3,4);
    QCOMPARE(false,testTact->getIsCorrect());
    testTact->appendFractionContainer(* new FractionsContainer(128,testTact->getUnitOfTime()));
    testTact->appendFractionContainer(* new FractionsContainer(64,testTact->getUnitOfTime()));
    QCOMPARE(true,testTact->getIsCorrect());
    testTact->setQuantityFraction(4);
    testTact->clearTact();
    testTact->appendFractionContainer(* new FractionsContainer(64,testTact->getUnitOfTime()));
    testTact->appendFractionContainer(* new FractionsContainer(64,testTact->getUnitOfTime()));
    testTact->appendFractionContainer(* new FractionsContainer(64,testTact->getUnitOfTime()));
    testTact->appendFractionContainer(* new FractionsContainer(64,testTact->getUnitOfTime()));
    QCOMPARE(true,testTact->getIsCorrect());
    testTact->setSingleDote(1);
    testTact->setFractionContainer(3,* new FractionsContainer(32,testTact->getUnitOfTime()));
    QCOMPARE(true,testTact->getIsCorrect());
    testTact->appstartFractionContainer(* new FractionsContainer(128, testTact->getUnitOfTime()));
    QCOMPARE(false,testTact->getIsCorrect());
    delete testTact;
}
//TODO: check accuracy of time in fractions OR realise this in player
//TODO: try make copy of the fractions
void TestsTest::testSetAccentFromTact(){
    Tact* testTact= new Tact(120,3,4);
    testTact->appendFractionContainer(* new FractionsContainer(128,testTact->getUnitOfTime()));
    testTact->setAccent(0,2,120);
    QCOMPARE(120,testTact->getFractionContainer(0).getNote(2)->getAccent());
    delete testTact;
}
void TestsTest::testSetTactKey(){
    Tact* testTact = new Tact(120,4,4);
    testTact->appendFractionContainer(* new FractionsContainer(64,testTact->getUnitOfTime()));
    testTact->setNote(0,2,30);
    QCOMPARE(30,testTact->getFractionContainer(0).getNote(2)->getSoundKey());
}
void TestsTest::testCopyTact(){
    Tact* testTact= new Tact(120,3,4);
    testTact->appendFractionContainer(* new FractionsContainer(128,testTact->getUnitOfTime()));
    testTact->appendFractionContainer(* new FractionsContainer(64,testTact->getUnitOfTime()));
    testTact->setNote(1,3,4);
    testTact->setNote(0,4,32);
    Tact* tactCopy= new Tact(*testTact);
    tactCopy->setNote(0,4,20);
    QCOMPARE(testTact->getMaxSize(),tactCopy->getMaxSize());
    QCOMPARE(4,tactCopy->getFractionContainer(1).getNote(3)->getSoundKey());
    QCOMPARE(20,tactCopy->getFractionContainer(0).getNote(4)->getSoundKey());
    QCOMPARE(32,testTact->getFractionContainer(0).getNote(4)->getSoundKey());
    delete tactCopy;
    delete testTact;
}

//TODO: refact repetitive variables.
QTEST_APPLESS_MAIN(TestsTest)

#include "tst_teststest.moc"
