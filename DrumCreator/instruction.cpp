#include "instruction.h"
#include "ui_instruction.h"

instruction::instruction(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::instruction)
{
    ui->setupUi(this);
    this->setWindowIcon(QIcon(":img/images/logo.png"));
    this->setWindowTitle("Инструкция");
}

instruction::~instruction()
{
    delete ui;
}

void instruction::on_nextButton_clicked()
{
    ui->stackedWidget->currentWidget()->hide();
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex()+1);
    ui->stackedWidget->currentWidget()->show();

}



void instruction::on_pushButton_2_clicked()
{
    ui->stackedWidget->currentWidget()->hide();
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex()-1);
    ui->stackedWidget->currentWidget()->show();


}

void instruction::on_prevButton_clicked()
{
    ui->stackedWidget->currentWidget()->hide();
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex()-1);
    ui->stackedWidget->currentWidget()->show();
}
