#include "player.h"

void Player::setRunning(bool value)
{
    running = value;
}

void Player::setTactId(int value)
{
    tactId = value;
}

void Player::setFractionId(int value)
{
    fractionId = value;
}

Player::Player()
{

}

void Player::run(QVector<Tact *> tacts_, QMidiOut* midiOut_, QMidiEvent* midiEvent_)
{

    running=true;
    for(int i=tactId;i<tacts_.size();i++) {
      if (running == false) break;
      if (tacts_[i]->getIsCorrect()) {
        QVector < FractionsContainer* > playFractions =
          tacts_[i]->getFractionContainers();
          if(i!=tactId) fractionId=0;
        for (int j = fractionId; j < playFractions.size(); j++) {
            if (running == false) break;
            emit setPlay(tacts_[i]->getId(),j);
          if (!playFractions.value(j)->isPause()) {
            for (int k = 0; k < 6; k++) {
              if (running == false) break;
              if (playFractions.value(j)->getNote(k)->value() != 0) {
                midiEvent_->setNote(playFractions.value(j)->getNote(k)->value());
                midiEvent_->setVelocity(playFractions.value(j)->getNote(k)->getAccent());
                midiOut_->sendEvent(*midiEvent_);
              }
            }

          }
         QThread::msleep(playFractions.value(j)->getTime() * 1000);
        }
      } else {
        QThread::msleep(tacts_[i]->getUnitOfTime() *
          tacts_[i]->getMaxSize() * 1000 * tacts_[i]->getUnitOfTime());
      }
    }
    emit finished();
}

