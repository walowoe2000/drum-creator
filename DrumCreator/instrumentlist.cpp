#include "instrumentlist.h"
#include "ui_instrumentlist.h"

instrumentList::instrumentList(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::instrumentList)
{
    ui->setupUi(this);
    this->setWindowTitle("Список инструментов");
    this->setWindowIcon(QIcon(":img/images/logo.png"));
}

instrumentList::~instrumentList()
{
    delete ui;
    emit destroyed();
}
