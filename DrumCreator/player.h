#ifndef PLAYER_H
#define PLAYER_H
#include "src/note.h"
#include "src/tact.h"
#include "QThread"
#include <QMidiOut.h>
#include <QMidiFile.h>
class Player: public QObject
{
      Q_OBJECT
    bool running;
    int tactId=0;
    int fractionId=0;
public:
    Player();
    void setRunning(bool value);

    void setTactId(int value);

    void setFractionId(int value);

public slots:
    void run(QVector<Tact*>,QMidiOut*,QMidiEvent*);
   signals:
    void finished();
    void setPlay(int,int);
};

#endif // PLAYER_H
