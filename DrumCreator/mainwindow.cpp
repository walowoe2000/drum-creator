#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    makeSongWidget();
    this->setWindowIcon(QIcon(":img/images/logo.png"));
    this->setWindowTitle("Drum Creator");
}

MainWindow::~MainWindow()
{

    delete ui;
}

void MainWindow::makeSongWidget()
{
    songWidget_=new Song(ui->compositionName->text(),ui->compositionTempo->value(),4,4,observer_);
    connect(songWidget_, &Song::playFinished, this, &MainWindow::readyToPlay);
    songWidget_->setObserver(observer_);
    songWidget_->setAlignment(Qt::AlignLeft);
    ui->scrollAreaWidgetContents->setLayout(songWidget_);
    ui->scrollArea->setWidgetResizable(true);
}

void MainWindow::makeNewFile()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("New File"),
                                                    "/home",
                                                    tr("*.dcff"));
    file= new QFile(fileName);
    file->open(QIODevice::WriteOnly);
    file->close();
}

void MainWindow::prepareSongWidget()
{
    delete songWidget_;
    songWidget_=new Song(observer_);
    connect(songWidget_, &Song::playFinished, this, &MainWindow::readyToPlay);
    songWidget_->setAlignment(Qt::AlignLeft);
    ui->scrollAreaWidgetContents->setLayout(songWidget_);
    ui->scrollArea->setWidgetResizable(true);
}

void MainWindow::setDescriptionValues()
{
    ui->compositionName->setText(songWidget_->getName());
    ui->compositionTempo->setValue(songWidget_->getTempo());
}

void MainWindow::on_changeNameAction_triggered()
{
    ui->compositionName->setFocus();
}

void MainWindow::on_changeTempoAction_triggered()
{
    ui->compositionTempo->setFocus();
}

void MainWindow::on_newFileAction_triggered()
{
    makeNewFile();
}

void MainWindow::on_compositionTempo_valueChanged(int arg1)
{
    songWidget_->setTempo(arg1);
}

void MainWindow::on_compositionName_textChanged(const QString &arg1)
{
    songWidget_->setName(arg1);
}

void MainWindow::on_appentTactAction_triggered()
{
   songWidget_->addTact();

}

void MainWindow::on_addTactButton_clicked()
{
    songWidget_->addTact();
}

void MainWindow::on_stopButton_clicked()
{
    ui->playButton->setEnabled(true);
    songWidget_->setEnabled(true);
    ui->menubar->setEnabled(true);
    ui->addTactButton->setEnabled(true);
    songWidget_->stopSong();

}

void MainWindow::on_playButton_clicked()
{
    ui->playButton->setEnabled(false);
    ui->pauseButton->setEnabled(true);
    songWidget_->setEnabled(false);
    ui->menubar->setEnabled(false);
    ui->addTactButton->setEnabled(false);
     songWidget_->playFullSong();
}

void MainWindow::on_pauseButton_clicked()
{
    ui->pauseButton->setEnabled(false);
    songWidget_->setEnabled(true);
    ui->menubar->setEnabled(true);
    ui->addTactButton->setEnabled(true);
     songWidget_->pauseSong();
}

void MainWindow::readyToPlay()
{
    ui->playButton->setEnabled(true);
    ui->pauseButton->setEnabled(false);
    songWidget_->setEnabled(true);
    ui->menubar->setEnabled(true);
    ui->addTactButton->setEnabled(true);

}

void MainWindow::on_saveAction_triggered()
{
    if(file==nullptr){
        makeNewFile();
    }
    songWidget_->writeFile(file->fileName());
}

void MainWindow::on_openFileAction_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
           tr("Open Address Book"), "",
           tr("Address Book (*.dcff)"));
    this->setWindowTitle(fileName);
    if(!fileName.isEmpty()){
        prepareSongWidget();
        file= new QFile(fileName);
        file->open(QIODevice::ReadOnly);
        file->close();
        songWidget_->readFile(fileName);
    }
}

void MainWindow::on_radioWhole_clicked()
{
    observer_->setNoteSize(256);
}

void MainWindow::on_radioSemi_clicked()
{
    observer_->setNoteSize(128);
}

void MainWindow::on_radioQuart_clicked()
{
    observer_->setNoteSize(64);
}

void MainWindow::on_radioEight_clicked()
{
    observer_->setNoteSize(32);
}

void MainWindow::on_radioSixTeen_clicked()
{
    observer_->setNoteSize(16);
}

void MainWindow::on_radioThirtyTwo_clicked()
{
    observer_->setNoteSize(8);
}

void MainWindow::on_radioNotDOte_clicked()
{
    observer_->setDoteType(0);
}

void MainWindow::on_radioSingleDote_clicked()
{
    observer_->setDoteType(1);
}

void MainWindow::on_radioDoubleDote_clicked()
{
    observer_->setDoteType(2);
}

void MainWindow::on_playAction_triggered()
{
    ui->playButton->click();
}

void MainWindow::on_stopAction_triggered()
{
    ui->stopButton->click();
}

void MainWindow::on_instruction_triggered()
{
    if(instructionWindow_==nullptr) instructionWindow_=new instruction();
    instructionWindow_->show();
}

void MainWindow::on_listAction_triggered()
{
    if(listWindow_==nullptr) listWindow_=new instrumentList();
    listWindow_->show();
}

void MainWindow::on_moreAction_triggered()
{
    if(aboutWin_==nullptr) aboutWin_=new aboutWindow();
    aboutWin_->show();
}

