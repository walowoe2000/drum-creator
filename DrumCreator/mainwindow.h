#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QFileDialog"
#include "QGridLayout"
#include "QFile"
#include "src/song.h"
#include "QThread"
#include "instrumentlist.h"
#include "aboutwindow.h"
#include "instruction.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_changeNameAction_triggered();

    void on_changeTempoAction_triggered();

    void on_newFileAction_triggered();

    void on_compositionTempo_valueChanged(int arg1);

    void on_compositionName_textChanged(const QString &arg1);

    void on_appentTactAction_triggered();

    void on_stopButton_clicked();

    void on_saveAction_triggered();

    void on_openFileAction_triggered();

    void on_radioWhole_clicked();

    void on_radioSemi_clicked();

    void on_radioQuart_clicked();

    void on_radioEight_clicked();

    void on_radioSixTeen_clicked();

    void on_radioThirtyTwo_clicked();

    void on_radioNotDOte_clicked();

    void on_radioSingleDote_clicked();

    void on_radioDoubleDote_clicked();

    void on_playButton_clicked();

    void on_pauseButton_clicked();

    void readyToPlay();

    void on_addTactButton_clicked();

    void on_playAction_triggered();

    void on_stopAction_triggered();

    void on_instruction_triggered();

    void on_listAction_triggered();

    void on_moreAction_triggered();

private:
    void makeSongWidget();
    void makeNewFile();
    void prepareSongWidget();
    void setDescriptionValues();
    QFile* file=nullptr;
    Song* songWidget_;
    Observer* observer_=new Observer();
    instrumentList* listWindow_=nullptr;
    aboutWindow* aboutWin_=nullptr;
    instruction* instructionWindow_=nullptr;
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
