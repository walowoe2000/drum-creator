#include "observer.h"

int Observer::noteSize() const
{
    return noteSize_;
}

int Observer::doteType() const
{
    return doteType_;
}

void Observer::setDoteType(int doteType)
{
    doteType_ = doteType;
}

void Observer::setNoteSize(int noteSize)
{
    noteSize_ = noteSize;
}

int Observer::accentValue() const
{
    return accentValue_;
}

void Observer::setAccentValue(int accentValue)
{
    accentValue_ = accentValue;
}
Observer::Observer()
{

}
