#include "tact.h"

Tact::Tact(int tempo, int quantity, int length,int id) {
  tactTempo_=new QSpinBox;
  tactTempo_->setRange(60,280);
  tactTempo_->setValue(tempo);
  selfDestructButton_=new QPushButton;
  selfDestructButton_->setStyleSheet(":enabled { color:  white; background-color: #072844;  border: none; } :disabled { color: white; background-color: #072844; }");
  selfDestructButton_->setText("X");
  selfDestructButton_->setFixedSize(35,30);
  toolsButton_=new QPushButton;
  toolsButton_->setText("more");
  toolsButton_->setFixedSize(40,25);
  id_=new QLabel();
  id_->setMaximumSize(40,40);
  addButton= new QPushButton("+");
  addButton->setStyleSheet(":enabled { color:  white; background-color: #072844;  border: none; } :disabled { color: white; background-color: #072844; }");
  quantityFraction_= new QComboBox;
  for(int i=2;i<16;i++)
      quantityFraction_->addItem(QString::number(i));
  quantityFraction_->setCurrentIndex(2);
  lenghtFraction_=new QComboBox;
  lenghtFraction_->addItem("4");
  lenghtFraction_->addItem("8");
  removeButton= new QPushButton("-");
  removeButton->setStyleSheet(":enabled { color:  white; background-color: #072844; border: none; } :disabled { color: white; background-color: #072844; }");
  connect(tactTempo_, QOverload<int>::of(&QSpinBox::valueChanged),
      [=](int i){ this->setTactTempo(i); });
  connect(lenghtFraction_,&QComboBox::currentTextChanged, this,
          [=]() { this->setMaxSize(); });
  connect(quantityFraction_, &QComboBox::currentTextChanged, this,
          [=]() { this->setMaxSize(); });
  connect(addButton, &QPushButton::clicked, this,
          [=]() { this->appendFractionContainer(); });
  connect(toolsButton_, &QPushButton::clicked, this,
          [=]() { this->toolPush(); });
  connect(removeButton, &QPushButton::clicked, this,
          [=]() { this->removeFraction(fractions_.size()-1); });
  connect(selfDestructButton_, &QPushButton::clicked, this,
          [=]() { this->triggerRemoving(); });
  addButton->setMaximumSize(35,250);
  removeButton->setFixedSize(35,260);
  lenghtFraction_ ->setCurrentText(QString::number(length));
  quantityFraction_ ->setCurrentText(QString::number(quantity));
  setMaxSize();
  id_->setText(QString::number(id));
  unitOfTime_ = (60 / double(tempo)) / 8;
  checkCorrect();
  //TODO: if tact is not correct, just ignore.
  fractions_.append(new FractionsContainer(64,unitOfTime_,0,id_->text().toInt()));
  left->addWidget(id_);
  left->addWidget(toolsButton_);
  left->addWidget(tactTempo_);
  left->addWidget(quantityFraction_);
  left->addWidget(lenghtFraction_);
  left->addWidget(addButton);
  center->addLayout(fractions_[0]);
  right->addWidget(selfDestructButton_);
  right->addWidget(removeButton);
  this->addLayout(left);
  this->addLayout(center);
  this->addLayout(right);
  this->tactUpdated();
}
Tact::Tact() {
  fractions_.append(new FractionsContainer(256, 124.4,0,id_->text().toInt()));
  this->addLayout(fractions_[0]);
  //TODO: add user param into constructor
}

Tact::Tact(const Tact & newTact, int id) {
  tactTempo_->setValue(newTact.getTactTempo());
  lenghtFraction_->setCurrentText(QString::number(newTact.getLenghtFraction()));
  quantityFraction_->setCurrentText(QString::number(newTact.getQuantityFraction()));
  unitOfTime_ = (60 / double(tactTempo_->value())) / 8;
  setMaxSize();
  id_->setText(QString::number(id));
  for (int i = 0; i < newTact.getFractionContainers().size(); i++) {
    fractions_.append(newTact.getFractionContainer(i));
    fractions_[i]->setId(i);
    fractions_[i]->setIdTact(id_->text().toInt());
  }
  for (int i = 0; i < this->fractions_.size(); i++) {
    this->addLayout(fractions_[i]);
  }
  checkCorrect();
}

Tact::~Tact() {

}

unsigned short Tact::getTactTempo() const {
  return tactTempo_->value();
}

void Tact::setTactTempo(unsigned short value) {
  tactTempo_->setValue(value);
  unitOfTime_ = (60 / double(tactTempo_->value())) / 8;
  for(int i=0;i<fractions_.size();i++){
       fractions_[i]->changeTime(unitOfTime_);
  }
  this->tactUpdated();
}

unsigned short Tact::getQuantityFraction() const {
  return quantityFraction_->currentText().toInt();
}

void Tact::setQuantityFraction(unsigned short value) {
  quantityFraction_->setCurrentText(QString::number(value));
  setMaxSize();
  this->tactUpdated();
}

unsigned short Tact::getLenghtFraction() const {
  return lenghtFraction_->currentText().toInt();
}

void Tact::setLenghtFraction(unsigned short value) {
  lenghtFraction_->setCurrentText(QString::number(value));
  setMaxSize();
  this->tactUpdated();
}

QDataStream & Tact::wFile(QDataStream & stream) {

  stream << isCorrect << unitOfTime_ << tactTempo_->value() << quantityFraction_->currentText().toInt() << lenghtFraction_->currentText().toInt() << maxSize_ << fractions_.size();
  for (int i = 0; i < fractions_.size(); i++) {
    fractions_[i]->wFile(stream);
  }

  return stream;
}

QDataStream & Tact::rFile(QDataStream & stream, int id) {
  int vectorSize;
  id_->setText(QString::number(id));
  int quantity;
  int length;
  int tempo;
  stream >> isCorrect >> unitOfTime_ >> tempo >> quantity >> length >> maxSize_ >> vectorSize;
  tactTempo_->setValue(tempo);
  quantityFraction_->setCurrentText(QString::number(quantity));
  lenghtFraction_->setCurrentText(QString::number(length));
  this->removeFraction(0);
  for (int i = 0; i < vectorSize; i++) {
    FractionsContainer* wFraction= new FractionsContainer();
    wFraction->rFile(stream, i, id_->text().toInt());
    fractions_.append(wFraction);
  }
  for (int i = 0; i < this->fractions_.size(); i++) {
    center->addLayout(fractions_[i]);
  }
  return stream;
}

int Tact::getId() const
{
    return id_->text().toInt();
}

void Tact::setId(int id)
{
    id_->setText(QString::number(id));
    for(int i=0;i<fractions_.size();i++)
        fractions_[i]->setIdTact(id_->text().toInt());
}

void Tact::setObserver(Observer *observer)
{
    observer_ = observer;
}

void Tact::setMaxSize() {
    int quantity=quantityFraction_->currentText().toInt();
  int length=lenghtFraction_->currentText().toInt();
    if (length == 4) {
        maxSize_ = quantity * 64;
    checkCorrect();
  }
  else if (length == 8) {
    maxSize_ = quantity * 32;
    checkCorrect();
  }
  this->tactUpdated();
  return;
}

bool Tact::getIsPlay() const
{
    return isPlay;
}

void Tact::checkCorrect() {
    int checkSize = maxSize_;
  for (FractionsContainer* var: fractions_) {
    checkSize -= var->getFractionSize();
  }
  isCorrect = checkSize == 0;
  QString style="color: ";
  style+=isCorrect?"white;":"red;";
  id_->setStyleSheet(style);
}

int Tact::getCurrentFraction()
{
    for(int i=0;i<fractions_.size();i++){
        if(fractions_[i]->getIsPlay()==true){
            return i;
        }
    }
    return 0;
}

bool Tact::getIsCorrect() const {
  return isCorrect;
}

void Tact::setTactDimention(int quantity, int lenght) {
  lenghtFraction_->setCurrentText(QString::number(lenght));
  quantityFraction_->setCurrentText(QString::number(quantity));
  setMaxSize();
}

void Tact::setCurrentPlay(int index)
{
    QString style="border-bottom: 2px solid rgb(255, 170, 0); padding-bottom: 3px;";
    isPlay=true;
    fractions_[index]->setCurrentPlay(style);
    if(index>0){
        fractions_[index-1]->deleteCurrentPlay();
    }
}

void Tact::removeLastCurrentPlay()
{
    if(fractions_.last()->getIsPlay()==true){
    isPlay=false;
    fractions_.last()->deleteCurrentPlay();
    }
}

void Tact::setFractionContainer(int index, FractionsContainer* newFraction) {
  fractions_[index] = newFraction;
  this->addLayout(newFraction);
  checkCorrect();
}

void Tact::appendFractionContainer(FractionsContainer* newFraction) {
  fractions_.append(newFraction);
  this->addLayout(newFraction);
  checkCorrect();
}

void Tact::appendFractionContainer()
{
    fractions_.append(new FractionsContainer(observer_->noteSize(),unitOfTime_,fractions_.size(),id_->text().toInt()));
    fractions_.last()->setDote(observer_->doteType());
    center->addLayout(fractions_.last());
    checkCorrect();
}

void Tact::tactUpdated()
{
    signalToSong();
}

void Tact::hideTempo(bool hide)
{

    if(hide==true)
        tactTempo_->hide();
    else
        tactTempo_->show();
}

void Tact::hideSize(bool hide)
{
    if(hide==true){
        lenghtFraction_->hide();
        quantityFraction_->hide();
    }else{
        lenghtFraction_->show();
        quantityFraction_->show();
    }
}

void Tact::toolPush(){
    if(tactTempo_->isHidden()||lenghtFraction_->isHidden()){
        tactTempo_->show();
        lenghtFraction_->show();
        quantityFraction_->show();
    }
    else{
        tactTempo_->hide();
        lenghtFraction_->hide();
        quantityFraction_->hide();
    }
}

void Tact::triggerRemoving()
{
    this->selfRemove();
}

void Tact::removeCurrentPlay()
{
    isPlay=false;
    for(int i=0;i<fractions_.size();i++)
        fractions_[i]->deleteCurrentPlay();
}

void Tact::setEnabled(bool value)
{
    addButton->setEnabled(value);
    removeButton->setEnabled(value);
    toolsButton_->setEnabled(value);
    selfDestructButton_->setEnabled(value);
    quantityFraction_->setEnabled(value);
    lenghtFraction_->setEnabled(value);
    tactTempo_->setEnabled(value);

}
void Tact::appstartFractionContainer(FractionsContainer* newFraction) {
  fractions_.push_front(newFraction);
  center->addLayout(newFraction);
  checkCorrect();
}

void Tact::removeFraction(int fractionIndex) {
  if (!fractions_.isEmpty()) fractions_.remove(fractionIndex);
  delete center->itemAt(fractionIndex);
  checkCorrect();
}


void Tact::clearTact() {
    QLayoutItem* item;
       while ( ( item = left->layout()->takeAt( 0 ) ) != NULL )
       {
           delete item->widget();
           delete item;
       }
       delete left->layout();
       while ( ( item = right->layout()->takeAt( 0 ) ) != NULL )
       {
           delete item->widget();
           delete item;
       }
       delete right->layout();
       while ( ( item = center->layout()->takeAt( 0 ) ) != NULL )
       {
           delete item->widget();
           delete item;
       }
       delete center->layout();

}

void Tact::setSingleDote(int fractionIndex) {
  fractions_[fractionIndex]->setSingleDote();
  checkCorrect();
}

void Tact::setDoubleDote(int fractionIndex) {
  fractions_[fractionIndex]->setDoubleDote();
  checkCorrect();
}

void Tact::removeDote(int fractionIndex) {
  fractions_[fractionIndex]->removeDote();
  checkCorrect();
}

void Tact::setAccent(int fractionIndex, int noteIndex, int accentValue) {
  fractions_[fractionIndex]->setAccent(noteIndex, accentValue);
}

int Tact::getMaxSize() {
  return maxSize_;
}

double Tact::getUnitOfTime() {
  return unitOfTime_;
}

QVector < FractionsContainer* > Tact::getFractionContainers() const {
  return fractions_;
}

void Tact::setNote(int fractionIndex, int noteIndex, int noteValue) {
  fractions_[fractionIndex]->setNoteKey(noteIndex, noteValue);
}

FractionsContainer* Tact::getFractionContainer(int index) const {
  return (fractions_)[index];
}

//TODO set DEFINE values for notes size
