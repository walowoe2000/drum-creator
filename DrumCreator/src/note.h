#ifndef NOTE_H
#define NOTE_H
#include "QDebug"
#include "QVector"
#include "QDataStream"
#include "QSpinBox"
class Note : public QSpinBox
{
    private:
        enum Accent{
            ACCENT_GHOST=90,
            ACCENT_STANDART=100,
            ACCENT_SPECIAL=120
        };
        Accent accent_=ACCENT_STANDART;
        int    soundKey_=0;
        //int    id_;
        int    idFraction_;
        int    idTact_;
    public:
        Note();
        Note(int soundKey, int idFracton, int idTact);
        Note(const Note &old);
        int getSoundKey() const;
        void setSoundKey(int soundKey);
        int getAccent() const;
        void setAccent(int accent);
        //TODO: Make methods for every accents
        QDataStream& wFile (QDataStream& stream);
        QDataStream& rFile(QDataStream& stream, int idFraction, int idTact);

        int getIdFraction() const;
        void setIdFraction(int value);

        int getIdTact() const;
        void setIdTact(int idTact);
};

#endif // NOTE_H
