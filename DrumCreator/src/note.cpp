#include "note.h"


void Note::setAccent(int accent) {
  if (accent == 90) accent_ = ACCENT_GHOST;
  else if (accent == 100) accent_ = ACCENT_STANDART;
  else if (accent == 120) accent_ = ACCENT_SPECIAL;
  return;
}

QDataStream & Note::rFile(QDataStream & stream, int idFraction, int idTact) {
  idFraction_=idFraction;
  idTact_=idTact;
  int value;
  stream >> value;
  this->setValue(value);
  stream >> value;
  setAccent(value);
  return stream;
}

QDataStream & Note::wFile(QDataStream & stream) {

  stream << this->value() << getAccent();
  return stream;
}

int Note::getIdFraction() const
{
    return idFraction_;
}

void Note::setIdFraction(int idFraction)
{
    idFraction_ = idFraction;
}

int Note::getIdTact() const
{
    return idTact_;
}

void Note::setIdTact(int idTact)
{
    idTact_ = idTact;
}

Note::Note() {

}

Note::Note(int soundKey, int idFraction, int idTact) {
    this->setValue(soundKey);
    this->idFraction_=idFraction;
    this->idTact_=idTact;
    this->setButtonSymbols(QAbstractSpinBox::NoButtons);
}

Note::Note(const Note & old) {
    this->setValue(old.value());
    setAccent(old.getAccent());
}

int Note::getAccent() const {
  if (accent_ == ACCENT_GHOST) return 90;
  else if (accent_ == ACCENT_STANDART) return 100;
  else if (accent_ == ACCENT_SPECIAL) return 120;
}
