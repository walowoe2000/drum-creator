#ifndef DESCRIPTIONFILE_H
#define DESCRIPTIONFILE_H

#include "QString"
#include "QDataStream"
class DescriptionFile
{
    private:
        unsigned short int tempo_=120;
        QString            name_="NoName";

    public:
        DescriptionFile();
        DescriptionFile(unsigned short int tempo,QString name);
        unsigned short int getTempo() const;
        void setTempo(unsigned short int tempo);
        QString getName() const;
        void setName(const QString &value);
        QDataStream& wFile (QDataStream& stream);
        QDataStream& rFile(QDataStream& stream);
};

#endif // DESCRIPTIONFILE_H
