#include "song.h"


void Song::setObserver(Observer *value)
{
    observer_ = value;
}

Song::Song() {
    descriptionFile_=new DescriptionFile(120,"f");
     midiOut_=new QMidiOut;
     midiEvent_=new QMidiEvent;
     midiEvent_->setType(QMidiEvent::NoteOn);
     midiEvent_->setVoice(0);
     midiEvent_->setNote(0);
     midiOut_->connect("128:0");
     player= new Player;
     playThread=new QThread(this);
     player->moveToThread(playThread);
     playThread->start();
}

Song::Song(Observer *observer)
{
    observer_=observer;
    descriptionFile_=new DescriptionFile();
    midiOut_=new QMidiOut;
    midiEvent_=new QMidiEvent;
    midiEvent_->setType(QMidiEvent::NoteOn);
    midiEvent_->setVoice(0);
    midiEvent_->setNote(0);
    midiOut_->connect("128:0");
    player= new Player;
    playThread=new QThread();
    player->moveToThread(playThread);
    connect(playThread, &QThread::started, player,
            [=]() { player->run(tacts_,midiOut_,midiEvent_); });
    connect(this, &Song::stopPlay, playThread,
            [=]() { playThread->quit(); });
    connect(player, &Player::finished, this,
            [=]() { this->readyToPlay(); });

    connect(player, &Player::setPlay, this, &Song::setCurrentPlay);
}

Song::Song(QString name, int tempo, int quantityFraction, int lenghtFraction,Observer* observer){
    observer_=observer;
    Tact* newTact= new Tact(tempo, quantityFraction, lenghtFraction,0);
    newTact->setObserver(observer_);
    connect(newTact, &Tact::signalToSong, this,
            [=]() {this->updateTactsData();});
    connect(newTact, &Tact::selfRemove, this,
            [=]() {removeTact(newTact->getId());});
    tacts_.append(newTact);
    this->addLayout(tacts_[0]);
    descriptionFile_=new DescriptionFile(tempo,name);
    midiOut_=new QMidiOut;
    midiEvent_=new QMidiEvent;
    midiEvent_->setType(QMidiEvent::NoteOn);
    midiEvent_->setVoice(0);
    midiEvent_->setNote(0);
    midiOut_->connect("128:0");
    player= new Player;
    playThread=new QThread();
    player->moveToThread(playThread);
    connect(playThread, &QThread::started, player,
            [=]() { player->run(tacts_,midiOut_,midiEvent_); });
    connect(this, &Song::stopPlay, playThread,
            [=]() { playThread->quit(); });
    connect(player, &Player::finished, this,
            [=]() { this->readyToPlay(); });

    connect(player, &Player::setPlay, this, &Song::setCurrentPlay);

}

void Song::setEnabled(bool value){
    for(int i=0;i<tacts_.size();i++)
    tacts_[i]->setEnabled(value);
}
void Song::setName(QString newName) {
  descriptionFile_->setName(newName);
}

void Song::setTempo(int newTempo) {
  descriptionFile_->setTempo(newTempo);
}

void Song::playFullSong() {
    if (!tacts_.isEmpty()) {
        int currentTact=0;
        int currentFraction=0;
        for(int i=0;i<tacts_.size();i++){
           if(tacts_[i]->getIsPlay()==true){
               currentTact=i;
               currentFraction=tacts_[i]->getCurrentFraction();
           }
        }
           player->setTactId(currentTact);
           player->setFractionId(currentFraction);
           this->setEnabled(false);
           playThread->start();
        }else{
        emit playFinished();
    }
  midiEvent_->setNote(0);

}

void Song::addTact() {
  int tempo=descriptionFile_->getTempo();
  int quant=4;
  int length=4;
  if (!tacts_.isEmpty()){
      tempo = tacts_.last()->getTactTempo();
      quant = tacts_.last()->getQuantityFraction();
      length = tacts_.last()->getLenghtFraction();
  }
  Tact* newTact = new Tact(tempo, quant, length,tacts_.size());
  newTact->setObserver(observer_);
  connect(newTact, &Tact::selfRemove, this,
          [=]() {removeTact(newTact->getId());});
  connect(newTact, &Tact::signalToSong, this,
          [=]() {this->updateTactsData();});
  tacts_.append(newTact);
  this->addLayout(tacts_.last());
  this->updateTactsData();
}
//TODO:: user data to append
void Song::testDatas() {

}

void Song::setTact(int index, Tact* newTact) {
  tacts_[index] = newTact;
}

void Song::insertTact(int index, Tact* newTact) {
  tacts_.insert(index, newTact);
}

void Song::insertTacts(int index, QVector < Tact* > newTacts) {
  for (int i = newTacts.size() - 1; i >= 0; i--) {
    tacts_.insert(index, newTacts.value(i));
  }
}

void Song::removeTact(int index) {
    qDebug()<<"tryREmove";
  tacts_[index]->clearTact();
  tacts_.remove(index);
  delete this->itemAt(index);
  checkId();
}

void Song::setTactDimention(int index, int quantity, int length) {
  tacts_[index]->setTactDimention(quantity, length);
}

void Song::clearTact(int index) {
  //tacts_[index]->clearTact();
}

void Song::setTactTempo(int index, unsigned short value) {
  tacts_[index]->setTactTempo(value);
}

void Song::setLenghtFraction(int index, unsigned short value) {
  tacts_[index]->setLenghtFraction(value);
}

void Song::setQuantityFraction(int index, unsigned short value) {
  tacts_[index]->setQuantityFraction(value);
}

void Song::removeFraction(int indexTact, int indexFraction) {
  tacts_[indexTact]->removeFraction(indexFraction);
}

void Song::setSingleDote(int indexTact, int fractionIndex) {
  tacts_[indexTact]->setSingleDote(fractionIndex);
}

void Song::setDoubleDote(int indexTact, int fractionIndex) {
  tacts_[indexTact]->setDoubleDote(fractionIndex);
}

void Song::removeDote(int indexTact, int fractionIndex) {
  tacts_[indexTact]->removeDote(fractionIndex);
}

void Song::setFractionContainer(int indexTact, int indexFraction, FractionsContainer* newFraction) {
  tacts_[indexTact]->setFractionContainer(indexFraction, newFraction);
}

void Song::appendFractionContainer(int indexTact, FractionsContainer* newFraction) {
  tacts_[indexTact]->appendFractionContainer(newFraction);
}

void Song::appstartFractionContainer(int indexTact, FractionsContainer* newFraction) {
  tacts_[indexTact]->appstartFractionContainer(newFraction);
}

void Song::setNote(int tactIndex, int fractionIndex, int noteIndex, int noteValue) {
  tacts_[tactIndex]->setNote(fractionIndex, noteIndex, noteValue);
}

void Song::setAccent(int tactIndex, int fractionIndex, int noteIndex, int accentValue) {
  tacts_[tactIndex]->setAccent(fractionIndex, noteIndex, accentValue);
}

void Song::newFile(QString fileName) {
  QFile file1(fileName);
  file1.open(QIODevice::WriteOnly);
  file1.close();
}

void Song::writeFile(QString fileName) {
  QFile file(fileName);
  QDataStream stream( & file);
  file.open(QIODevice::WriteOnly);
  descriptionFile_->wFile(stream);
  stream << tacts_.size();
  for (int i = 0; i < tacts_.size(); i++) {
    tacts_[i]->wFile(stream);
  }
  file.close();
}

void Song::readFile(QString fileName) {
  QFile file(fileName);
  QDataStream stream( & file);
  file.open(QIODevice::ReadOnly);
  descriptionFile_->rFile(stream);
  int tactsSize;
  stream >> tactsSize;
  for(int i=0;i<tactsSize;i++){
      Tact* newTact = new Tact(120, 4, 4,i);
      newTact->setObserver(observer_);
      connect(newTact, &Tact::selfRemove, this,
              [=]() {removeTact(newTact->getId());});
      connect(newTact, &Tact::signalToSong, this,
              [=]() {this->updateTactsData();});
      tacts_.append(newTact);
      this->addLayout(tacts_.last());
  }
  for (int i = 0; i < tactsSize; i++) {

    tacts_[i]->rFile(stream,i);
    tacts_[i]->checkCorrect();
  }
  file.close();
  this->updateTactsData();
}

void Song::updateTactsData()
{
    int tempo=tacts_[0]->getTactTempo();
    int quantity=tacts_[0]->getQuantityFraction();
    int length=tacts_[0]->getLenghtFraction();
    if(tacts_.size()>1){
        for(int i=1;i<tacts_.size();i++){
            if(tacts_[i]->getTactTempo()!=tempo){
            tempo=tacts_[i]->getTactTempo();
            tacts_[i]->hideTempo(false);
            }else{
                tacts_[i]->hideTempo(true);
            }
            if(tacts_[i]->getQuantityFraction()!=quantity||tacts_[i]->getLenghtFraction()!=length){
                quantity=tacts_[i]->getQuantityFraction();
                length=tacts_[i]->getLenghtFraction();
                tacts_[i]->hideSize(false);
            }else{
                tacts_[i]->hideSize(true);
            }
        }
    }
}

void Song::checkId(){
    for(int i=0;i<tacts_.size();i++)
        tacts_[i]->setId(i);
}

void Song::readyToPlay(){
    playThread->quit();
    int index=0;
    for(int i=0;i<tacts_.size();i++){
        if(tacts_[i]->getIsCorrect()==true)
            index=i;
    }
    tacts_[index]->removeLastCurrentPlay();
    this->setEnabled(true);
    emit playFinished();
}

void Song::stopSong()
{
    player->setRunning(false);
    for(int i=0;i<tacts_.size();i++){
        if(tacts_[i]->getIsPlay()==true){
            tacts_[i]->removeCurrentPlay();
        }
    }
}

void Song::pauseSong()
{
    player->setRunning(false);
    this->setEnabled(true);
}

void Song::setCurrentPlay(int tactId, int fractionId)
{

    tacts_[tactId]->setCurrentPlay(fractionId);
    if(tactId>0){
        tacts_[tactId-1]->removeLastCurrentPlay();
    }
}
QVector<Tact*> *Song::getTacts(){
    return &tacts_;
}

QString Song::getName(){
    return descriptionFile_->getName();
}

int Song::getTempo(){
    return descriptionFile_->getTempo();
}

Song::~Song() {
  delete descriptionFile_;
  int size = tacts_.size();
    if(size>0)
  for(int i=0;i<size;i++)
      removeTact(0);
  midiOut_->disconnect();
  delete midiOut_;
  delete midiEvent_;
}
