#ifndef TACT_H
#define TACT_H

#include "fractionscontainer.h"
#include "QPushButton"
#include "QLabel"
#include "QComboBox"
#include "observer.h"

class Tact : public QHBoxLayout
{
    Q_OBJECT
    private:
        double                      unitOfTime_;
        unsigned short int          maxSize_;
        QVector<FractionsContainer*> fractions_;
        QLabel* id_;
        QPushButton* addButton;
        QPushButton* removeButton;
        QPushButton* toolsButton_;
        QPushButton* selfDestructButton_;
        QComboBox* quantityFraction_;
        QComboBox* lenghtFraction_;
        QSpinBox* tactTempo_;
        QVBoxLayout* left=new QVBoxLayout;
        QHBoxLayout* center=new QHBoxLayout;
        QVBoxLayout* right=new QVBoxLayout;
        Observer* observer_;
        void setMaxSize();
        bool isCorrect;
        bool isPlay;
    public:
        Tact();
        Tact(const Tact &newTact,int id);
        ~Tact();
        Tact(int tempo, int quantity, int length,int id);
        void checkCorrect();
        int getCurrentFraction();
        void setTactDimention(int quantity, int length);
        void setCurrentPlay(int index);
        void removeLastCurrentPlay();
        void setFractionContainer(int,FractionsContainer*);
        void appendFractionContainer(FractionsContainer*);
        void appstartFractionContainer(FractionsContainer*);
        void removeFraction(int fractionIndex);
        void clearTact();
        void setSingleDote(int fractionIndex);
        void setDoubleDote(int fractionIndex);
        void removeDote(int fractionIndex);
        void setAccent(int fractionIndex, int noteIndex,int accentValue);
        int getMaxSize();
        double getUnitOfTime();
        QVector<FractionsContainer*> getFractionContainers()const;
        FractionsContainer* getFractionContainer(int)const;
        void setNote(int fractionIndex,int noteIndex,int noteValue);
        unsigned short getTactTempo() const;
        void setTactTempo(unsigned short value);
        bool getIsCorrect() const;
        unsigned short getQuantityFraction() const;
        void setQuantityFraction(unsigned short value);
        unsigned short getLenghtFraction() const;
        void setLenghtFraction(unsigned short value);
        QDataStream& wFile (QDataStream& stream);
        QDataStream& rFile(QDataStream& stream, int id);
        int getId() const;
        void setId(int id);
        void appendFractionContainer();
        void tactUpdated();
        void hideTempo(bool hide);
        void hideSize(bool hide);
        void toolPush();
        void triggerRemoving();
        void removeCurrentPlay();
        void setEnabled(bool value);
        void setObserver(Observer *observer);

        bool getIsPlay() const;

signals:
        void signalToSong();
        void selfRemove();

};
//TODO: make correction, for situation, if file will be failed
#endif // TACT_H
