#include "fractionscontainer.h"
#include "QIcon"
FractionsContainer::FractionsContainer() {
  for (int i = 0; i < MAX_NOTES_ARRAY_SIZE; i++) {
    notes_.append(Note(0,id_,idTact_));
  }
  noteType_=new QLabel;
  noteType_->setMaximumSize(30,25);
  noteType_->setPixmap(QPixmap(":img/images/"+QString::number(1)+".png"));
  noteType_->setScaledContents(true);
  this->addWidget(noteType_);
  for(int i=0;i<MAX_NOTES_ARRAY_SIZE;i++){
      notes_[i].setButtonSymbols(QAbstractSpinBox::NoButtons);
      this->addWidget(&notes_[i]);
  }
}

FractionsContainer::~FractionsContainer()
{
    delete noteType_;
}

FractionsContainer::FractionsContainer(int fractionsSize, double unitOfTime, int id, int idTact) {
  fractionSize_ = fractionsSize;
  realFractionSize_ = fractionSize_;
  changeTime(unitOfTime);
  realTime_ = time_;
  id_=id;
  idTact_=idTact;
  noteType_=new QLabel;
  noteType_->setMaximumSize(20,25);
  if(fractionsSize==256)
  noteType_->setMaximumSize(30,25);
  QIcon* pic=new QIcon;
  pic->addFile(":img/images/"+QString::number(fractionsSize)+".png",QSize(30,25),QIcon::Disabled,QIcon::Off);
  QPixmap pix=pic->pixmap(QSize(30,24));
  noteType_->setPixmap(pix);
  noteType_->setScaledContents(true);
  this->addWidget(noteType_);
  for (int i = 0; i < MAX_NOTES_ARRAY_SIZE; i++) {
    notes_.append(Note(0,id_,idTact_));
  }
  for(int i=0;i<MAX_NOTES_ARRAY_SIZE;i++){
       notes_[i].setButtonSymbols(QAbstractSpinBox::NoButtons);
      this->addWidget(&notes_[i]);
  }
}
void FractionsContainer::setProperties(QVector < Note > newNotes, int dote) {
  if (dote == 0) removeDote();
  else if (dote == 1) setSingleDote();
  else if (dote == 2) setDoubleDote();

  for (int i = 0; i < MAX_NOTES_ARRAY_SIZE; i++) {
    notes_[i].setValue(newNotes[i].value());
    notes_[i].setAccent(newNotes[i].getAccent());
    notes_[i].setIdTact(idTact_);
    notes_[i].setIdFraction(id_);
  }
}

int FractionsContainer::getIdTact() const
{
    return idTact_;
}

void FractionsContainer::setIdTact(int idTact)
{
    idTact_ = idTact;
    for(int i=0;i<notes_.size();i++)
        notes_[i].setIdTact(idTact_);
}

int FractionsContainer::getId() const
{
    return id_;
}

void FractionsContainer::setId(int id)
{
    id_ = id;
    for(int i=0;i<notes_.size();i++)
        notes_[i].setIdFraction(id_);
}

bool FractionsContainer::getIsPlay() const
{
    return isPlay;
}

void FractionsContainer::setIsPlay(bool value)
{
    isPlay = value;
}

void FractionsContainer::setEnabled(bool value)
{
    foreach (Note var, notes_) {
        var.setEnabled(value);
    }
}

QVector < Note > * FractionsContainer::getNotes() {
    return &notes_;
}

void FractionsContainer::setNote(int index, Note newNote) {
    notes_[index].setValue(newNote.value());
  notes_[index].setAccent(newNote.getAccent());
  notes_[index].setIdTact(idTact_);
  notes_[index].setIdFraction(id_);
  if (pause_ == true) pause_ = false;
  //TODO: correct = operator

}

void FractionsContainer::setNoteKey(int index, int soundKey) {
    notes_[index].setValue(soundKey);
}

void FractionsContainer::setCurrentPlay(QString style){
        isPlay=true;
        noteType_->setStyleSheet(style);
}

void FractionsContainer::setAccent(int index, int accentValue) {
  notes_[index].setAccent(accentValue);
}

void FractionsContainer::removeNote(int index) {
  notes_[index].setValue(0);
}

int FractionsContainer::getFractionSize() {
  return fractionSize_;
}

double FractionsContainer::getTime() {
  return time_;
}

void FractionsContainer::changeTime(double newUnitOfTime) {
  time_ = (fractionSize_ * newUnitOfTime) / 8;
}

void FractionsContainer::removeDote() {
  time_ = realTime_;
  fractionSize_ = realFractionSize_;
  dote_ = DOTE_NOTDOT;
}

void FractionsContainer::setDote(int doteValue) {
  QString pixName=QString::number(realFractionSize_);
  switch (doteValue) {
      case 0: {
          removeDote();
          break;
      }
      case 1: {
          setSingleDote();
          pixName+="d";
          break;
      }
      case 2: {
          setDoubleDote();
          pixName+="dd";
          break;
      }
  }
  noteType_->setPixmap(QPixmap(":img/images/"+pixName+".png"));
}

void FractionsContainer::setSingleDote() {
  switch (dote_) {
  case DOTE_NOTDOT:
    time_ += time_ * 0.5;
    fractionSize_ += fractionSize_ * 0.5;
    dote_ = DOTE_SINGLEDOT;
    break;
  case DOTE_SINGLEDOT:
    break;
  case DOTE_DOUBLEDOT:
    time_ = realTime_ + (realTime_ * 0.5);
    fractionSize_ = realFractionSize_ + (realFractionSize_ * 0.5);
    dote_ = DOTE_SINGLEDOT;
    break;
  }
}

void FractionsContainer::setDoubleDote() {
  switch (dote_) {
  case DOTE_NOTDOT:
    time_ += time_ * 0.75;
    fractionSize_ += fractionSize_ * 0.75;
    dote_ = DOTE_DOUBLEDOT;
    break;
  case DOTE_SINGLEDOT:
    time_ += (time_ / 3) / 2;
    fractionSize_ += (fractionSize_ / 3) / 2;
    dote_ = DOTE_DOUBLEDOT;
    break;
  case DOTE_DOUBLEDOT:
    break;
  }
}

void FractionsContainer::setPause() {
  if (pause_ == false) {
    pause_ = true;
    for (int i = 0; i < MAX_NOTES_ARRAY_SIZE; i++) {
      notes_[i].setValue(0);
    }
  }
}

bool FractionsContainer::isPause() {
  return pause_;
}

int FractionsContainer::getRealSize() {
    return realFractionSize_;
}

void FractionsContainer::deleteCurrentPlay()
{
    isPlay=false;
    noteType_->setStyleSheet("border: ");

}

Note * FractionsContainer::getNote(int index) {
  return &notes_[index];
}

int FractionsContainer::getDot() {
  if (dote_ == DOTE_NOTDOT) return 0;
  if (dote_ == DOTE_SINGLEDOT) return 1;
  if (dote_ == DOTE_DOUBLEDOT) return 2;
  return 0;
}


QDataStream & FractionsContainer::wFile(QDataStream & stream) {
  stream  << realTime_ << realFractionSize_ << time_ << fractionSize_ << pause_ << getDot();
  for (int i = 0; i < MAX_NOTES_ARRAY_SIZE; i++) {
    notes_[i].wFile(stream);
  }
  return stream;
}
QDataStream & FractionsContainer::rFile(QDataStream & stream,int id, int idTact) {
  int dote;
  id_=id;
  idTact_=idTact;
  stream >> realTime_ >> realFractionSize_ >> time_ >> fractionSize_ >> pause_;
  stream >> dote;
  noteType_->setMaximumSize(20,25);
  if(realFractionSize_==256)
  noteType_->setMaximumSize(30,25);
  noteType_->setPixmap(QPixmap(":img/images/"+QString::number(realFractionSize_)+".png"));
  setDote(dote);
  for (int i = 0; i < MAX_NOTES_ARRAY_SIZE; i++) {
    notes_[i].rFile(stream,id_, idTact_);
  }
  return stream;
}
