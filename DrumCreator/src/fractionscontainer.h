#ifndef FRACTIONSCONTAINER_H
#define FRACTIONSCONTAINER_H
#define MAX_NOTES_ARRAY_SIZE 6
#include "note.h"
#include "QVBoxLayout"
#include "QLabel"
class FractionsContainer : public QVBoxLayout
{
    protected:
        enum Dote{
            DOTE_NOTDOT=0,
            DOTE_SINGLEDOT=1,
            DOTE_DOUBLEDOT=2
        };
        Dote          dote_=DOTE_NOTDOT;
        bool          pause_=false;
        QVector<Note> notes_;
        double        time_;
        double        realTime_;
        int           fractionSize_;
        int           realFractionSize_;
        int           id_;
        int           idTact_;
        QLabel*       noteType_;
        bool          isPlay=false;
    public:
        QVector<Note>* getNotes();
        FractionsContainer();
        ~FractionsContainer();
        FractionsContainer(int fractionsSize, double unitOfTime, int id, int idTact);
        void setNote(int index,Note oldNote);
        void setNoteKey(int index,int soundKey);
        void setCurrentPlay(QString style);
        void setAccent(int index,int accent);
        void removeNote(int index);
        void setProperties(QVector<Note> newNotes, int dote);
        int getFractionSize();
        double getTime();
        void changeTime(double unitOfTime);
        Note* getNote(int index);
        void setSingleDote();
        void setDoubleDote();
        void removeDote();
        void setDote(int dote);
        void setPause();
        bool isPause();
        int getRealSize();
        void deleteCurrentPlay();
        int getDot();
        QDataStream& wFile (QDataStream& stream);
        QDataStream& rFile(QDataStream& stream, int id, int idTact);
        int getIdTact() const;
        void setIdTact(int idTact);
        int getId() const;
        void setId(int id);
        bool getIsPlay() const;
        void setIsPlay(bool value);
        void setEnabled(bool value);
};

#endif // FRACTIONSCONTAINER_H
