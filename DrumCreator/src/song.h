#ifndef SONG_H
#define SONG_H
#include "tact.h"
#include "descriptionfile.h"
#include <QMidiOut.h>
#include <QMidiFile.h>
#include <QElapsedTimer>
#include <QThread>
#include "QFile"
#include "player.h"
class Song : public QHBoxLayout
{
    Q_OBJECT
    DescriptionFile* descriptionFile_;
    QVector<Tact*>   tacts_;
    QMidiOut*        midiOut_;
    QMidiEvent*      midiEvent_;
    Observer*        observer_;
    QThread*         playThread;
    Player*          player;
    public:
        Song();
        Song(Observer* observer);
        Song(QString name, int tempo, int quantityFraction, int lenghtFraction,Observer* observer);
        void playFullSong();
        void setName(QString newName);
        void setTempo (int newTempo);
        void addTact();
        void testDatas();
        void setTact(int index,Tact* newTact);
        void insertTact(int index, Tact* newTact);
        void insertTacts(int index, QVector<Tact*> newTacts);
        void removeTact(int index);
        void setTactDimention(int index,int quantity, int length);
        void clearTact(int index);
        void setTactTempo(int index,unsigned short value);
        void setLenghtFraction(int index, unsigned short value);
        void setQuantityFraction(int index, unsigned short value);
        void removeFraction(int indexTact, int indexFraction);
        void setSingleDote(int indexTact,int fractionIndex);
        void setDoubleDote(int indexTact,int fractionIndex);
        void removeDote(int indexTact,int fractionIndex);
        void setFractionContainer(int indexTact,int indexFraction,FractionsContainer* newFraction);
        void appendFractionContainer(int indexTact,FractionsContainer* newFraction);
        void appstartFractionContainer(int indexTact,FractionsContainer* newFraction);
        void setNote(int tactIndex,int fractionIndex,int noteIndex,int noteValue);
        void setAccent(int tactIndex,int fractionIndex, int noteIndex,int accentValue);
        void newFile(QString fileName);
        void writeFile (QString fileName);
        void readFile (QString fileName);
        void updateTactsData();
        void checkId();
        void readyToPlay();
        void stopSong();
        void pauseSong();
        void setCurrentPlay(int tactId, int fractionId);
        QVector<Tact*>* getTacts();
        QString getName();
        int getTempo();
        void setEnabled(bool value);
        ~Song();

        void setObserver(Observer *value);
signals:
        void stopPlay();
        void playFinished();
};

#endif // SONG_H
