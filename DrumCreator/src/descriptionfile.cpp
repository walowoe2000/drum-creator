#include "descriptionfile.h"

DescriptionFile::DescriptionFile() {

}

DescriptionFile::DescriptionFile(unsigned short int tempoValue, QString nameValue): tempo_(tempoValue), name_(nameValue) {

}

unsigned short DescriptionFile::getTempo() const {
  return tempo_;
}

void DescriptionFile::setTempo(unsigned short tempo) {
  tempo_ = tempo;
}

QString DescriptionFile::getName() const {
  return name_;
}

void DescriptionFile::setName(const QString & name) {
  name_ = name;
}

QDataStream & DescriptionFile::rFile(QDataStream & stream) {
  stream >> name_ >> tempo_;
  return stream;
}

QDataStream & DescriptionFile::wFile(QDataStream & stream) {
  stream << name_ << tempo_;
  return stream;
}
