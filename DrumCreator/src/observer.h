#ifndef OBSERVER_H
#define OBSERVER_H


class Observer
{
    int noteSize_=64;
    int doteType_=0;
    int accentValue_=100;
public:
    Observer();
    int noteSize() const;
    void setNoteSize(int noteSize);
    int doteType() const;
    void setDoteType(int doteType);
    int accentValue() const;
    void setAccentValue(int accentValue);
};

#endif // OBSERVER_H
