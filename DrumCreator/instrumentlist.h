#ifndef INSTRUMENTLIST_H
#define INSTRUMENTLIST_H

#include <QDialog>

namespace Ui {
class instrumentList;
}

class instrumentList : public QDialog
{
    Q_OBJECT

public:
    explicit instrumentList(QWidget *parent = nullptr);
    ~instrumentList();

private:
    Ui::instrumentList *ui;
};

#endif // INSTRUMENTLIST_H
