#include "aboutwindow.h"
#include "ui_aboutwindow.h"

aboutWindow::aboutWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::aboutWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("О программе");
    this->setWindowIcon(QIcon(":img/images/logo.png"));
    ui->label->setPixmap(QPixmap(":img/images/logo.png"));
}

aboutWindow::~aboutWindow()
{
    delete ui;
}
